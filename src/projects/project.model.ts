export interface Project {
    name: string
    cost: string
    owner: string
}


export interface ProjectDoc extends Document {
    name: string;
    cost: string;
    owner: string;
  }

export class ProjectDto {
    name: string;
    cost: string;
    owner: string;
}