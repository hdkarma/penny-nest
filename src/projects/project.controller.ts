import { UseGuards } from '@nestjs/common';
import { Body, Controller, Get, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ProjectDto } from './project.model';
import { ProjectService } from './project.service';

@Controller('projects')
export class ProjectController {

    constructor(private projectService: ProjectService) {

    }

    @Post() 
    @UseGuards(AuthGuard('jwt'))
    async createNewProject(@Body() createproject: ProjectDto){
        return await this.projectService.create(createproject);
    }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    async getAllProjects() {
        return await this.projectService.getAllProjects();
    }

}