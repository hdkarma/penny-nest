import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { ProjectSchema } from './projectSchema';

@Module({
  imports: [
    MongooseModule.forFeature([{name: 'Project', schema: ProjectSchema}]),

  ],
  exports: [ProjectService],
  controllers: [ProjectController],
  providers: [ProjectService]
})
export class ProjectsModule {}
