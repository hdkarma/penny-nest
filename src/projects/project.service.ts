import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Project, ProjectDto } from './project.model';

@Injectable()
export class ProjectService {

  constructor(@InjectModel('Project') private projectModel: Model<Project>) {}

  async create(projectDto: ProjectDto) {

    let createdProject = new this.projectModel(projectDto);
    return await createdProject.save();

  }

  async findOneByProject(project) : Promise<any>{
    return await this.projectModel.findOne({project: project})
  }


  async getAllProjects() : Promise<any>{
    return await this.projectModel.find()
  }

}