import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { ProjectDoc } from './project.model';

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


export const ProjectSchema = new mongoose.Schema<ProjectDoc>({
    name: {
        type: String,
        unique: true,
        required: true
    },
    cost: {
        type: String,
        unique: false,
        required: true
    },
    owner: {
        type: String,
        required: true
    }
});
