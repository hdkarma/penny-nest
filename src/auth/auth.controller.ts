import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from '../users/dto/login-user.dto'

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) {

    }

    @Post() 
    async login(@Body() loginUserDto: LoginUserDto){
        let login =  await this.authService.validateUserByPassword(loginUserDto);

        if(login) {return login} else return null
    }

}