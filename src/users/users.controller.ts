import { Controller, Get, Post, Body, UseGuards, Patch, Header, Req } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) {

    }

    @Post() 
    async create(@Body() createUserDto: CreateUserDto) {
        return await this.usersService.create(createUserDto);
    }

    @Patch() 
    async updatePassword(@Body() userData: CreateUserDto) {
        let users = await this.usersService.findOneByEmail(userData.email)
        users.password = userData.password_new
        return await users.save()
    }

    // @Get('test')
    // @UseGuards(AuthGuard())
    // testAuthRoute(){
    //     return {
    //         message: 'You did it!'
    //     }
    // }

}