import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { UserDoc } from './user.interface';

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


export const UserSchema = new mongoose.Schema<UserDoc>({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

// NOTE: Arrow functions are not used here as we do not want to use lexical scope for 'this'
UserSchema.pre('save', async function(next){
  
    const rounds = 10; // What you want number for round paasword
  
    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;
    next()
  

}); 

UserSchema.methods.checkPassword = function(attempt, callback){
    bcrypt.compare(attempt, this.password, (err, isMatch) => {
        if(err) return callback(err);
        callback(null, isMatch);
    });

};