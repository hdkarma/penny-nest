export interface User {
    email: string
}


export interface UserDoc extends Document {
    name: string;
    email: string;
    password: string;
    dateJoined: string;
    matchPassword: (pw: string) => Promise<boolean>
  }