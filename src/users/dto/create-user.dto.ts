export class CreateUserDto {
    readonly email: string;
    readonly password: string;
    readonly password_new: string;
}